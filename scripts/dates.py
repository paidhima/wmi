import datetime


class Dates(object):

    def __init__(self):
        self.now = datetime.datetime.now()
        self.scanDate = self.now.strftime('%Y-%m-%d %H:%M:%S')
        self.fileNameDate = self.now.strftime("%Y-%m-%d %H'%M'%S")

    def dateForEventHistory(self, daysBack):
        if daysBack == -1:
            self.eventHistory = None
        elif daysBack == 0:
            self.eventHistory = 'all'
        else:
            desiredDate = self.now - datetime.timedelta(days=daysBack)
            self.eventHistory = desiredDate.strftime('%m/%d/%Y 00:00:00')
        return self.eventHistory
