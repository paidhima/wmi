import os
import sys
import yaml
import logging
import logging.config
from win32crypt import CryptProtectData, CryptUnprotectData
from config import defaults
from scanner import computer, filemanager, gethosts, getinput


script_path = os.path.dirname(os.path.abspath(__file__))
logging.config.fileConfig(os.path.join(script_path, 'config', 'logging.cfg'),
                          disable_existing_loggers=False)
logger = logging.getLogger(__name__)

try:
    with open(r'config\configuration.yml') as fin:
        CONFIG = yaml.load(fin, Loader=yaml.FullLoader)
except:
    logger.error('Unable to load configuration file. Does it exist?')
    sys.exit(-1)
else:
    logger.info('Configuration loaded')

getinput = getinput.GetUserInput()


class ScanManager(object):

    def __init__(self):
        self.scannedHosts = []
        self.bad_hosts = []
        self.hostSummary = {}
        logger.info('Begin scan process.')
        self.get_hosts()
        if not CONFIG['RUN_AS_LOCAL']:
            self.get_credentials()
        self.get_event_history()
        self.fileOutputPath = self.check_directories()

    def get_hosts(self):
        logger.info('Getting a list of hosts to scan.')
        if CONFIG['HOST_LIST']:
            nh = gethosts.GetHosts(hlist=CONFIG['HOST_LIST'])
        elif CONFIG['HOST_FILE']:
            nh = gethosts.GetHosts(hfile=CONFIG['HOST_FILE'])
        else:
            nh = gethosts.GetHosts()
        self.hosts = nh.hosts
        logger.info('Found {} hosts.'.format(len(self.hosts)))
        print(nh)
        self.nh = nh

    def get_credentials(self):
        import getpass
        logger.info('Getting credentials.')
        self.uname = CryptProtectData(
            input('Enter username: ').encode(), '', None, None, None, 0)
        self.pword = CryptProtectData(
            getpass.win_getpass().encode(), '', None, None, None, 0)
        logger.info('Credentials stored.')

    def get_event_history(self):
        logger.info('Getting event history.')
        self.eventHistory = getinput.ask(
            'Event history? (0=all, -1=none)', 'num')
        print('')
        logger.info('Event history set to: {}'.format(self.eventHistory))

    def check_directories(self):
        cwd = os.getcwd()
        fileOutputPath = os.path.join(cwd, CONFIG['SCAN_PATH'], defaults.FOLDER_MONTH,
                                      defaults.FOLDER_DAY, defaults.FOLDER_TIME)
        logger.info('Creating directories.')
        try:
            os.makedirs(fileOutputPath)
        except WindowsError as e:
            logger.warning(
                'Could not create folder.  May already exist: {}'.format(e))
        else:
            logger.info('Path {} created.'.format(fileOutputPath))
        return fileOutputPath

    def scan_all_hosts(self, hosts, retry=False):
        logger.info('Starting scan process.')
        for host in hosts:
            currHost = self.scan_host(host)
            if currHost:
                logger.info('Adding host summary data to hostSummary.')
                self.hostSummary.update(currHost.build_summary())
                self.write_host_to_file(currHost)
                self.scannedHosts.append(currHost)
            else:
                logger.info('Host was not scanned successfully.')
                if retry:
                    self.bad_hosts.append(host)
            logger.info('***** END {} *****'.format(host[1]))

    def scan_host(self, host):
        logger.info('***** {} *****'.format(host[1]))
        print('Host {}'.format(host[1].upper()))
        currHost = computer.Computer(host, defaults.SCAN_DATE)
        if not CONFIG['RUN_AS_LOCAL']:
            status = currHost.connect(CryptUnprotectData(self.uname, None, None, None, 0)[1],
                                      CryptUnprotectData(self.pword, None, None, None, 0)[1])
        else:
            status = currHost.connect()
        if not status:
            logger.error('Could not connect to host.')
            print('  No connection available.\n')
            return False

        def print_to_screen(obj, name, *args):
            print('  {}...'.format(name))
            try:
                result = obj(*args)
            except AttributeError:
                raise
            except Exception:
                print('    Failed!')

        self.tasks = (
            (currHost.get_system, ('System',)),
            (currHost.get_os, ('OS',)),
            (currHost.get_network, ('Network Adapters',)),
            (currHost.get_disks, ('Disks',)),
            (currHost.get_shares, ('Shares',)),
            (currHost.get_printers, ('Printers',)),
            (currHost.get_events, ('Events', self.eventHistory)),
        )
        for func, args in self.tasks:
            try:
                print_to_screen(func, *args)
            except Exception as e:
                logger.error('Error, skipping task {}: {}'.format(args[0], e))
        print('\n')
        return currHost

    def write_host_to_file(self, host):
        logger.info('Writing data to individual host file.')
        hostFile = filemanager.HostWorkbook(os.path.join(
            self.fileOutputPath, host.hostName + '.xls'))
        hostFile.add_host_data(host.summary)
        hostFile.add_sheet('Installed Programs')
        if host.installed_programs:
            hostFile.write_to_sheet(
                'Installed Programs', host.installed_programs.output())
        else:
            hostFile.write_to_sheet('Installed Programs', None)
        if host.events.count > 0:
            summary, complete = host.events.output()
            hostFile.add_sheet('Events Summary')
            hostFile.write_to_sheet('Events Summary', summary)
            hostFile.add_sheet('All Events')
            hostFile.write_to_sheet('All Events', complete)
        hostFile.finalize_book()
        logger.info('File complete.')


def copy_log(path):
    import shutil
    shutil.copy('log.txt', path)


def main():
    sm = ScanManager()
    sm.scan_all_hosts(sm.hosts, retry=True)
    sm.scan_all_hosts(sm.bad_hosts)
    fm = filemanager.SummaryWorkbook(
        os.path.join(sm.fileOutputPath, 'summary.xls'))
    summary = fm.build_summary(sm.hostSummary)
    fm.write_to_summary(summary)
    fm.finalize_book()
    copy_log(sm.fileOutputPath)
    import subprocess
    subprocess.Popen(['explorer.exe', sm.fileOutputPath])


if __name__ == "__main__":
    main()
