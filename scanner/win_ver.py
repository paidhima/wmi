"""
    Windows OS Version/Arch -> Uninstall Registry Location map
"""

VERSIONS = {
    '10.0': {'64': r'Software\Microsoft\Windows\CurrentVersion\Uninstall',
             '32': r'Software\Microsoft\Windows\CurrentVersion\Uninstall'},
    '6.3': {'64': r'Software\WoW6432Node\Microsoft\Windows\CurrentVersion\Uninstall',
            '32': r'Software\Microsoft\Windows\CurrentVersion\Uninstall'},
    '6.2': {'64': r'Software\WoW6432Node\Microsoft\Windows\CurrentVersion\Uninstall',
            '32': r'Software\Microsoft\Windows\CurrentVersion\Uninstall'},
    '6.1': {'64': r'Software\WoW6432Node\Microsoft\Windows\CurrentVersion\Uninstall',
            '32': r'Software\Microsoft\Windows\CurrentVersion\Uninstall'},
    '6.0': {'64': r'Software\Microsoft\Windows\CurrentVersion\Uninstall',
            '32': r'Software\Microsoft\Windows\CurrentVersion\Uninstall'},
    '5.2': {'64': r'Software\Microsoft\Windows\CurrentVersion\Uninstall',
            '32': r'Software\Microsoft\Windows\CurrentVersion\Uninstall'},
}
