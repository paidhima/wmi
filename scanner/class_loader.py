import json
import logging
import os


logger = logging.getLogger(__name__)
CLASS_DIR = os.path.join(os.path.dirname(
    os.path.abspath(__file__)), 'wmi_classes')


class WMIClass:

    def __init__(self, json_dict):
        self.name = json_dict['Name']
        self.properties = set(prop['Name'].lower()
                              for prop in json_dict['Properties'])
        self.load_class_properties(json_dict['Properties'])

    def load_class_properties(self, properties):
        for p in properties:
            if p['Name'] != 'Name':
                setattr(self, p['Name'].lower(), WMIClassProperty(p))


class WMIClassProperty:

    def __init__(self, p_dict):
        for p in p_dict:
            setattr(self, p.lower(), p_dict[p])


def load_classes():
    json_files = os.listdir(CLASS_DIR)
    class_names = [name.split('.')[0] for name in json_files]
    classes = {WMIClass(json.load(open(os.path.join(CLASS_DIR, json_file))))
               for json_file in json_files}
    return class_names, classes
