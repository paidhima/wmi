﻿$wmi_objects = Get-WmiObject -List | Where {$_.name -match "^Win32_"}

foreach ($obj in $wmi_objects) {
    $obj | ConvertTo-Json | Out-File "wmi_classes\\$($obj.name).json" -encoding ascii
}