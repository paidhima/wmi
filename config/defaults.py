import datetime

# --- DATES
TODAY = datetime.datetime.now()
SCAN_DATE = datetime.date.strftime(TODAY, '%Y-%m-%d %H:%M:%S')
FOLDER_MONTH = '{}-{}'.format(TODAY.year, str(TODAY.month).zfill(2))
FOLDER_DAY = '{}-{}-{}'.format(TODAY.year, str(TODAY.month).zfill(2),
                               str(TODAY.day).zfill(2))
FOLDER_TIME = "{}'{}'{}".format(str(TODAY.hour).zfill(2), 
                                str(TODAY.minute).zfill(2), 
                                str(TODAY.second).zfill(2))
# --- END DATES

PRINTER_STATE = {  0: 'On Line',
                   1: 'Paused',
                   2: 'Pending Deletion',
                   3: 'Error',
                   4: 'Paper Jam',
                   5: 'Paper Out',
                   6: 'Manual Feed',
                   7: 'Paper Problem',
                   8: 'Offline',
                   256: 'IO Active',
                   512: 'Busy',
                   1024: 'Printing',
                   2048: 'Output Bin Full',
                   4096: 'Not Available',
                   8192: 'Waiting',
                   6384: 'Processing',
                   32768: 'Initializing',
                   65536: 'Warming Up',
                   131072: 'Toner Low',
                   262144: 'No Toner',
                   524288: 'Page Punt',
                   1048576: 'User Intervention',
                   2097152: 'Out of Memory',
                   4194304: 'Door Open',
                   8388608: 'Server Unknown',
                   16777216: 'Power Save',
                   }

PRINTER_STATUS = { 1: 'Other',
                   2: 'Unknown',
                   3: 'Idle',
                   4: 'Printing',
                   5: 'Warming Up',
                   6: 'Stopped printing',
                   7: 'Offline'
                   }

PRINTER_ERROR_STATUS = { 0: 'Unknown',
                         1: 'Other',
                         2: 'No Error',
                         3: 'Low Paper',
                         4: 'No Paper',
                         5: 'Low Toner',
                         6: 'No Toner',
                         7: 'Door Open',
                         8: 'Jammed',
                         9: 'Offline',
                         10: 'Service Requested',
                         11: 'Output Bin Full'
                         }

PRINTER_EXTENDED_STATUS = { 0: 'Unknown',
                            1: 'Other',
                            2: 'No Error',
                            3: 'Low Paper',
                            4: 'No Paper',
                            5: 'Low Toner',
                            6: 'No Toner',
                            7: 'Door Open',
                            8: 'Jammed',
                            9: 'Service Requested',
                            10: 'Output Bin Full',
                            11: 'Paper Problem',
                            12: 'Cannot Print Page',
                            13: 'User Intervention Required',
                            14: 'Out of Memory',
                            15: 'Server Unknown'
                            }

OUTPUT_SPACES = [ [''], [''] ]